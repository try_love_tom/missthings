//
//  QueryHeader.h
//  MissThings
//
//  Created by ChangTom on 2015/9/21.
//  Copyright © 2015年 張子晏. All rights reserved.
//

#ifndef QueryHeader_h
#define QueryHeader_h

#define DAY_TO_SECONDS(t) 60 * 60 * 24 * t

#define TYPE_NSSTRING_0 @"DailySupplies"
#define TYPE_NSSTRING_1 @"ClothingAccessories"
#define TYPE_NSSTRING_2 @"CredentialsProperty"
#define TYPE_NSSTRING_3 @"Stationery"
#define TYPE_NSSTRING_4 @"ElectronicProducts"
#define TYPE_NSSTRING_5 @"Others"

#define TYPE_0 DailySupplies
#define TYPE_1 ClothingAccessories
#define TYPE_2 CredentialsProperty
#define TYPE_3 Stationery
#define TYPE_4 ElectronicProducts
#define TYPE_5 Others

#define QUERY_TYPES_NUM 6

typedef enum {
    TYPE_0 = 0,
    TYPE_1,
    TYPE_2,
    TYPE_3,
    TYPE_4,
    TYPE_5,
} types;

#endif /* QueryHeader_h */
