//
//  MissThing.h
//
//
//  Created by 張子晏 on 2015/9/13.
//
//

#import <GoogleMaps/GoogleMaps.h>
#import "Parse/Parse.h"

@interface MissThingsMarker : GMSMarker

@property PFObject *missThing;
@property UIImage *photo;

- (instancetype)init;
- (instancetype)initWithPFObject:(PFObject *)object;
- (void)setObjectWithoutLocation:(PFObject *)object;
- (void)getPhoto;

@end
