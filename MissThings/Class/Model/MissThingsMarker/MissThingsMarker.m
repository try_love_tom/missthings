//
//  MissThing.m
//
//
//  Created by 張子晏 on 2015/9/13.
//
//

#import "MissThingsMarker.h"

@implementation MissThingsMarker

- (instancetype)init {
    self = [super init];
    if (self) {
        self.missThing = [PFObject objectWithClassName:@"MissThings"];
        [self setAppearAnimation:kGMSMarkerAnimationPop];
    }
    return self;
}

- (instancetype)initWithPFObject:(PFObject *)object {
    self = [super init];
    if (self) {
        PFGeoPoint *location;
        
        self.missThing = object;
        location = object[@"location"];
        [self setPosition:CLLocationCoordinate2DMake(location.latitude, location.longitude)];
        [self setAppearAnimation:kGMSMarkerAnimationPop];
    }
    return self;
}

- (void)setObjectWithoutLocation:(PFObject *)object {
    PFGeoPoint *location = [PFGeoPoint geoPointWithLatitude:self.position.latitude longitude:self.position.longitude];
    
    self.missThing = object;
    self.missThing[@"location"] = location;
    [self setAppearAnimation:kGMSMarkerAnimationPop];
}

- (void)getPhoto {
    PFFile *imageFile = [self.missThing objectForKey:@"photo"];
    
    [imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        if (error) {
            NSLog(@"%@", [error description]);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        } else {
            self.photo = [UIImage imageWithData:data];
        }
    }];
}

@end
