//
//  AppDelegate.m
//  MissThings
//
//  Created by 張子晏 on 2015/8/15.
//  Copyright (c) 2015年 張子晏. All rights reserved.
//

#import "AppDelegate.h"
#import "Parse/Parse.h"
@import GoogleMaps;

#define GOOGLE_MAP_API_KEY @"AIzaSyDr-nRj15Ou4EzqtkJU8hfU9YXfDSzoC2I"
#define PARSE_APPLICATION_ID @"P7Mmyjt6DhYlR99jGhrN065XI3DvqOIfV3GIPO3m"
#define PARSE_CLIENT_KEY @"FHHrwpUKylXoD89uLju7aICzGmTEhNPCJZRIQL9P"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // Enables you to monitor your application's API usage, and ensures that Google can contact you about your application if necessary.
    [GMSServices provideAPIKey:GOOGLE_MAP_API_KEY];
    
    // Setup Parse.
    [Parse setApplicationId:PARSE_APPLICATION_ID clientKey:PARSE_CLIENT_KEY];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
