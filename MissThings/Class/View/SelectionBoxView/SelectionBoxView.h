//
//  SelectionBox.h
//  MissThings
//
//  Created by ChangTom on 2015/9/21.
//  Copyright © 2015年 張子晏. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectionBoxView : UIView

@property NSMutableArray *typeButtonArray;
@property NSMutableArray *timeButtonArray;
@property (weak, nonatomic) IBOutlet UIView *TypeView;

@end
