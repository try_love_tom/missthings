//
//  MarkerInfoView.h
//  MissThings
//
//  Created by 張子晏 on 2015/9/13.
//  Copyright (c) 2015年 張子晏. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParseUI/ParseUI.h"

@interface MarkerInfoView : UIView

@property (weak, nonatomic) IBOutlet PFImageView *photo;

@end
