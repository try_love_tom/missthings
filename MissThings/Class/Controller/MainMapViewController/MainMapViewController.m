//
//  MainMapViewController.m
//  MissThings
//
//  Created by 張子晏 on 2015/8/15.
//  Copyright (c) 2015年 張子晏. All rights reserved.
//

#import "MainMapViewController.h"
#import "MissThingsViewController.h"
#import "SVProgressHUD.h"
#import "Parse/Parse.h"
#import "GoogleMaps/GoogleMaps.h"
#import "MissThingsMarker.h"
#import "MarkerInfoView.h"
#import "QueryHeader.h"

typedef enum {
    addNewMissThings = 0,
} alertViewTag;

@interface MainMapViewController () <GMSMapViewDelegate, UIAlertViewDelegate> {
    GMSCameraPosition *ntustCamera;
    GMSMutablePath *drawPath;
    UIView *touchView;
    UIButton *drawButton;
    CLLocationCoordinate2D ntustCoordinate;
    NSMutableArray *thingsMarkerArray;
    BOOL isDrawing;
    
    // Query setting.
    NSDate *queryDate;
    NSMutableArray *queryTypes;
    BOOL isClothingAccessories;
    BOOL isDailySupplies;
    BOOL isCredentialsProperty;
    BOOL isStationery;
    BOOL isElectronicProducts;
    BOOL isOthers;
}

@property (strong, nonatomic) IBOutlet GMSMapView *googleMapView;

@end

@implementation MainMapViewController

NSString *queryTypeStrings[QUERY_TYPES_NUM] = { TYPE_NSSTRING_0, TYPE_NSSTRING_1, TYPE_NSSTRING_2, TYPE_NSSTRING_3, TYPE_NSSTRING_4, TYPE_NSSTRING_5 };

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Create a CLLocationCoordinate that set the coordinate at NTUST
    ntustCoordinate = CLLocationCoordinate2DMake(25.01355f, 121.5418f);
    
    // Create a GMSCameraPosition that tells the map to display the NTUSTcoordinate at zoom level 17.
    ntustCamera = [GMSCameraPosition cameraWithTarget:ntustCoordinate zoom:17];
    
    isDrawing = NO;
    
    // Initialize the drawPath
    drawPath = [[GMSMutablePath alloc] init];
    
    // Initialize the thingsMarker
    thingsMarkerArray = [[NSMutableArray alloc] init];
    
    // Initialize the touch view.
    touchView = [[UIView alloc] initWithFrame:self.view.frame];
    [touchView setBackgroundColor:[UIColor colorWithRed:0 green:217 blue:207 alpha:0.2]];
    [touchView setAlpha:0.5f];
    [self.view addSubview:touchView];
    [touchView setHidden:!isDrawing];
    
    // Initialize the draw button
    drawButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 75, 75)];
    [drawButton setImage:[UIImage imageNamed:@"Pon.png"] forState:UIControlStateNormal];
    [drawButton setCenter:CGPointMake(self.view.frame.size.width - 50, self.view.frame.size.height - 100)];
    [drawButton addTarget:self action:@selector(drawAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:drawButton];
    
    // Set google map delegate.
    [self.googleMapView setDelegate:self];
    
    // Set camera of the google map view.
    [self.googleMapView setCamera:ntustCamera];
    
    NSLog(@"%lf", [self.googleMapView.projection coordinateForPoint:self.googleMapView.center].latitude);
    
    // Controls whether the My Location dot and accuracy circle is enabled.
    [self.googleMapView setMyLocationEnabled:YES];
    
    // Set default date, 3 days.
    queryDate = [NSDate dateWithTimeIntervalSinceNow:-DAY_TO_SECONDS(3)];
    
    // Set default type.
    isDailySupplies = NO;
    isClothingAccessories = NO;
    isCredentialsProperty = NO;
    isStationery = NO;
    isElectronicProducts = NO;
    isOthers = NO;
    queryTypes = [[NSMutableArray alloc] initWithObjects:@"DailySupplies", @"ClothingAccessories", @"CredentialsProperty", @"Stationery", @"ElectronicProducts", @"Others", nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    MissThingsMarker *marker = (MissThingsMarker *)self.googleMapView.selectedMarker;
    MissThingsMarker *newMarker = [[MissThingsMarker alloc] initWithPFObject:marker.missThing];
    
    [marker setMap:nil];
    [newMarker setIcon:[UIImage imageNamed:@"1.png"]];
    [newMarker setInfoWindowAnchor:CGPointMake(0.75f, 1.0f)];
    [newMarker setMap:self.googleMapView];
    [newMarker getPhoto];
    [self.googleMapView setSelectedMarker:nil];
}

#pragma mark - UIAlertViewDldgate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    // Check which alert view.
    switch (alertView.tag) {
        case addNewMissThings:
            switch (buttonIndex) {
                case 0:
                    [self.googleMapView clear];
                    break;
                    
                case 1:
                    [self performSegueWithIdentifier:@"NewMissThingsSegue" sender:self];
                    break;
                    
                default:
                    break;
            }
            break;
            
        default:
            break;
    }
}

#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate {
    // Clear the google map marker.
    [self.googleMapView clear];
    
    // Clear thingsMarkerArray
    [thingsMarkerArray removeAllObjects];
    
    // Add new marker
    MissThingsMarker *marker = [[MissThingsMarker alloc] init];
    [marker setPosition:CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude)];
    [marker setIcon:[UIImage imageNamed:@"1.png"]];
    [marker setInfoWindowAnchor:CGPointMake(0.75f, 1.0f)];
    [marker setMap:self.googleMapView];
    
    // Add new miss thing
    PFObject *newMissThings = [PFObject objectWithClassName:@"MissThings"];
    
    [marker setObjectWithoutLocation:newMissThings];
    [self.googleMapView setSelectedMarker:marker];
    [thingsMarkerArray addObject:marker];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"!!" message:@"是否新增失物？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確認", nil];
    
    alertView.tag = addNewMissThings;
    [alertView show];
}

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    MissThingsMarker *missThingMarker = (MissThingsMarker *)marker;
    MarkerInfoView *infoView =  [[MarkerInfoView alloc] init];
    
    [infoView.photo setImage:missThingMarker.photo];
    return infoView;
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    [self performSegueWithIdentifier:@"NewMissThingsSegue" sender:self];
}

#pragma mark - Touch Event

// View Touch Event
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint point = [touch locationInView:touch.view];
    CLLocationCoordinate2D coordinate = [self.googleMapView.projection coordinateForPoint:point];
    
    // Clear the map.
    [self.googleMapView clear];
    
    // Clear path.
    [drawPath removeAllCoordinates];
    
    // Add path.
    [drawPath addCoordinate:coordinate];
    
    NSLog(@"TouchesBegin: %lf, %lf", coordinate.latitude, coordinate.longitude);
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint point = [touch locationInView:touch.view];
    CLLocationCoordinate2D coordinate = [self.googleMapView.projection coordinateForPoint:point];
    
    // Add path
    [drawPath addCoordinate:coordinate];
    
    // Add line
    GMSMutablePath *linePath = [[GMSMutablePath alloc] init];
    
    [linePath addCoordinate:[drawPath coordinateAtIndex:[drawPath count] - 1]];
    [linePath addCoordinate:[drawPath coordinateAtIndex:[drawPath count] - 2]];
    
    // Draw line
    GMSPolyline *line = [GMSPolyline polylineWithPath:linePath];
    
    [line setStrokeColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.3]];
    [line setStrokeWidth:3.0];
    [line setMap:self.googleMapView];
    
    NSLog(@"TouchesMoved: %lf, %lf", coordinate.latitude, coordinate.longitude);
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint point = [touch locationInView:touch.view];
    CLLocationCoordinate2D coordinate = [self.googleMapView.projection coordinateForPoint:point];
    
    // Claer last data.
    [thingsMarkerArray removeAllObjects];
    
    // Clear Google Map View.
    [self.googleMapView clear];
    
    // Add path.
    [drawPath addCoordinate:coordinate];
    
    // Add line.
    GMSMutablePath *linePath = [[GMSMutablePath alloc] init];
    
    [linePath addCoordinate:[drawPath coordinateAtIndex:[drawPath count] - 1]];
    [linePath addCoordinate:[drawPath coordinateAtIndex:0]];
    
    // Draw line.
    GMSPolyline *line = [GMSPolyline polylineWithPath:linePath];
    
    [line setStrokeColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.3]];
    [line setStrokeWidth:3.0];
    [line setMap: self.googleMapView];
    
    // Draw path
    GMSPolygon *polygon = [GMSPolygon polygonWithPath:drawPath];
    
    [polygon setFillColor:[UIColor colorWithRed:0 green:217 blue:207 alpha:0.1]];
    [polygon setMap:self.googleMapView];
    
    // Draw line.
    GMSPolyline *polygonLine = [GMSPolyline polylineWithPath:polygon.path];
    
    [polygonLine setStrokeColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.3]];
    [polygonLine setStrokeWidth:3.0];
    [polygonLine setMap:self.googleMapView];
    
    NSLog(@"TouchesEnded: %lf, %lf", coordinate.latitude, coordinate.longitude);
    
    [self queryWithPolygon:polygon];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint point = [touch locationInView:touch.view];
    CLLocationCoordinate2D coordinate = [self.googleMapView.projection coordinateForPoint:point];
    
    // Clear path.
    [drawPath removeAllCoordinates];
    
    NSLog(@"TouchesCancelled: %lf, %lf", coordinate.latitude, coordinate.longitude);
}

#pragma mark - Query

- (void)queryWithPolygon:(GMSPolygon *)polygon {
    // Query server data.
    
    PFQuery *queryMissThings = [PFQuery queryWithClassName:@"MissThings"];
    
    // Set query type.
    [queryTypes removeAllObjects];
    
    if (isDailySupplies) {
        [queryTypes addObject:TYPE_NSSTRING_0];
    }
    
    if (isClothingAccessories) {
        [queryTypes addObject:TYPE_NSSTRING_1];
    }
    
    if (isCredentialsProperty) {
        [queryTypes addObject:TYPE_NSSTRING_2];
    }
    
    if (isStationery) {
        [queryTypes addObject:TYPE_NSSTRING_3];
    }
    
    if (isElectronicProducts) {
        [queryTypes addObject:TYPE_NSSTRING_4];
    }
    
    if (isOthers) {
        [queryTypes addObject:TYPE_NSSTRING_5];
    }
    
    if ([queryTypes count]) {
        [queryMissThings whereKey:@"type" containedIn:queryTypes];
    }
    
    // Set query date.
    if (queryDate) {
        [queryMissThings whereKey:@"createdAt" greaterThanOrEqualTo:queryDate];
    }
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [self stopDraw];
    
    [queryMissThings findObjectsInBackgroundWithBlock:^(NSArray *missThingsArray, NSError *error) {
        if (error) {
            NSLog(@"%@", [error description]);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        } else {
            
            // Init Marker.
            for (PFObject *missThing in missThingsArray) {
                MissThingsMarker *marker = [[MissThingsMarker alloc] initWithPFObject:missThing];
                
                [marker setIcon:[UIImage imageNamed:@"1.png"]];
                [marker setInfoWindowAnchor:CGPointMake(0.75f, 1.0f)];
                [thingsMarkerArray addObject:marker];
            }
            
            // Set Marker.
            for (MissThingsMarker *marker in thingsMarkerArray) {
                if (GMSGeometryContainsLocation(marker.position, polygon.path, YES)) {
                    [marker getPhoto];
                    [marker setMap:self.googleMapView];
                }
            }
            
            [SVProgressHUD dismiss];
        }
    }];
}

#pragma mark - Action

// Draw Action
- (void)drawAction:(id)sender {
    if (isDrawing) {
        [self stopDraw];
    } else {
        [self startDraw];
    }
}

// Type Action
- (void)dailySuppliesAction {
    isDailySupplies = !isDailySupplies;
}

- (void)clothingAccessoriesAction {
    isClothingAccessories = !isClothingAccessories;
}

- (void)credentialsPropertyAction {
    isCredentialsProperty = !isCredentialsProperty;
}

- (void)stationeryAction {
    isStationery = !isStationery;
}

- (void)ElectronicProducts {
    isElectronicProducts = !isElectronicProducts;
}

- (void)othersAction {
    isOthers = !isOthers;
}

// Date Action

- (void)todayAction {
    queryDate = [NSDate dateWithTimeIntervalSinceNow:-DAY_TO_SECONDS(1)];
}

- (void)threeDaysAction {
    queryDate = [NSDate dateWithTimeIntervalSinceNow:-DAY_TO_SECONDS(3)];
}

- (void)aWeekAciton {
    queryDate = [NSDate dateWithTimeIntervalSinceNow:-DAY_TO_SECONDS(7)];
}

- (void)aMonthAciton {
    queryDate = [NSDate dateWithTimeIntervalSinceNow:-DAY_TO_SECONDS(31)];
}

- (void)aSemesterAction {
    queryDate = [NSDate dateWithTimeIntervalSinceNow:-DAY_TO_SECONDS(100)];
}

// Stop Draw
- (void)stopDraw {
    isDrawing = NO;
    [touchView setHidden:YES];
    [drawButton setImage:[UIImage imageNamed:@"Pon.png"] forState:UIControlStateNormal];
}

// Start Draw
- (void)startDraw {
    // Clear the map.
    [self.googleMapView clear];
    
    isDrawing = YES;
    [touchView setHidden:NO];
    [drawButton setImage:[UIImage imageNamed:@"Poff.png"] forState:UIControlStateNormal];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqual:@"NewMissThingsSegue"]) {
        if(self.googleMapView.selectedMarker) {
            id destinationController = segue.destinationViewController;
            MissThingsMarker *seletedMissThingMarker = (MissThingsMarker *)self.googleMapView.selectedMarker;
            PFObject *passingObject = seletedMissThingMarker.missThing;
            UIImage *passingImage = seletedMissThingMarker.photo;
            
            [destinationController setValue:passingObject forKey:@"missThing"];
            [destinationController setValue:passingImage forKey:@"photo"];
        }
    }
}

@end
