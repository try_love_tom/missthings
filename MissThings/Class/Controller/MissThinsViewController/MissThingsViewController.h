//
//  NewMissThingsViewController.h
//  
//
//  Created by 張子晏 on 2015/9/12.
//
//

#import <UIKit/UIKit.h>
#import "Parse/Parse.h"

@interface MissThingsViewController : UIViewController

@property PFObject *missThing;
@property UIImage *photo;
@property BOOL isEditMode;

@end
