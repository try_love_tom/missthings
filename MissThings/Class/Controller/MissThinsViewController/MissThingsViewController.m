//
//  NewMissThingsViewController.m
//
//
//  Created by 張子晏 on 2015/9/12.
//
//

#import "MissThingsViewController.h"
#import "DBCameraViewController.h"
#import "DBCameraContainerViewController.h"
#import "DBCameraSegueViewController.h"
#import "ParseUI/ParseUI.h"
#import "SVProgressHUD.h"

@interface MissThingsViewController () <DBCameraViewControllerDelegate>

@property (weak, nonatomic) IBOutlet PFImageView *thingsImageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation MissThingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (self.photo) {
        [self.thingsImageView setImage:self.photo];
    }
    [self.scrollView setContentSize:CGSizeMake(0, self.scrollView.frame.size.height)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Tap Gesture Action
- (IBAction)tapImageViewAction:(id)sender {
    [self openCamera];
}

//Use your captured image
#pragma mark - DBCameraViewControllerDelegate

- (void)camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata {
    [self.thingsImageView setImage:image];
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)dismissCamera:(id)cameraViewController{
    [self dismissViewControllerAnimated:YES completion:nil];
    [cameraViewController restoreFullScreenMode];
}

// Open camera in forceQuadCrop mode
- (void)openCamera {
    DBCameraViewController *cameraController = [DBCameraViewController initWithDelegate:self];
    [cameraController setForceQuadCrop:YES];
    
    DBCameraContainerViewController *container = [[DBCameraContainerViewController alloc] initWithDelegate:self];
    [container setCameraViewController:cameraController];
    [container setFullScreenMode];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:container];
    [nav setNavigationBarHidden:YES];
    [self presentViewController:nav animated:YES completion:nil];
}

// Dismiss Action
- (IBAction)dismissAction:(id)sender {
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    
    if (self.thingsImageView.image) {
        NSData *imageData = UIImageJPEGRepresentation(self.thingsImageView.image, 0.5f);
        PFFile *imageFile = [PFFile fileWithName:@"photo.png" data:imageData];
        self.missThing[@"photo"] = imageFile;
    }
    
    self.missThing[@"keyword"] = @"blue pen";
    self.missThing[@"type"] = @"";
    self.missThing[@"condition"] = @"Lost";
    self.missThing[@"track"] = @"A->B";
    self.missThing[@"finder"] = @"Tom";
    self.missThing[@"finderInformation"] = @"0975";
    self.missThing[@"condition"] = @"Lost";
    self.missThing[@"owner"] = @"Carol";
    self.missThing[@"ownerInformation"] = @"0966";
    [self.missThing saveInBackgroundWithBlock:^(BOOL success, NSError *error) {
        [SVProgressHUD dismiss];
        [self dismissViewControllerAnimated:YES completion:nil];
        if (error) {
            NSLog(@"%@", [error description]);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        } else {
            NSLog(@"Success");
        }
    }];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
