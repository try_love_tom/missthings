//
//  TabBarViewController.m
//  MissThings
//
//  Created by 張子晏 on 2015/8/15.
//  Copyright (c) 2015年 張子晏. All rights reserved.
//

#import "TabBarViewController.h"

@interface TabBarViewController ()

@end

@implementation TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //UITabBar.appearance().tintColor = UIColor.redColor()
    //[self.tabBar setTintColor:[UIColor colorWithRed:255 green:0 blue:0 alpha:1.0]];
    
    [self.tabBar.items[0] setImage:[[UIImage imageNamed:@"123"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [self.tabBar.items[0] setSelectedImage:[[UIImage imageNamed:@"123G"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    // Set to the middle view.
    [self setSelectedIndex:1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
